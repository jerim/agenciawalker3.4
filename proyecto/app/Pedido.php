<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $fillable = ['id','user_id','producto_id','cantidad','precio'];

    public function productos(){
        //return $this->belongsToMany(Producto::class)->withPivot('pedido_id','producto_id','cantidad','precio');
        return $this->belongsToMany('App\Producto','detalles_pedidos','pedido_id','producto_id','cantidad','precio');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function total(){
        $total = 0;

        foreach ($this->productos as $producto) {
           $total +=$producto->pivot->precio * $producto->pivot->cantidad;
        }

        return $total;
    }
}
