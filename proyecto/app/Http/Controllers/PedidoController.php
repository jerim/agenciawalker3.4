<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pedido;
use App\Producto;
class PedidoController extends Controller
{


    public function __construct() {

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pedidos = Pedido::all();
        return view('pedidos.index',['pedidos' =>$pedidos]);

    }


    public function show($id)
    {
        $pedido = Pedido::findOrFail($id);
        $productos = $pedido->productos;

        $pedido_id = $pedido->id;
        $total = $pedido->total();

        if($pedido->pagar == 0) {
            $pagar = "No pagado";
        } else {
            $pagar = "Pagado";
        }

        return view('pedidos.show',[
            'pedido'=>$pedido, 'total'=>$total,'pagar'=>$pagar]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pagar(Request $request,$id)
    {
        $pedido = Pedido::findOrFail($id);
        $pedido->pagar = 1;

        $pedido->save();
        return redirect('/pedidos');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generarPDFPedidos($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generarPDFPresupuesto($id)
    {

    }
}
