<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/inicio', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/logout','Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');

/*Rutas menú  */
Route::resource('agencia','ProductoController');


/*Rutas Cesta  */

Route::get('/tienda','CestaController@index');
Route::get('/tienda/vaciar','CestaController@vaciar');
Route::post('tienda','CestaController@store');
Route::get('/tienda/quitar/{id}','CestaController@quitar');
Route::get('/tienda/{id}','CestaController@añadir');

/*Rutas Pedidos  */
Route::resource('pedidos','PedidoController');
Route::post('/pedidos/{id}/pagar','PedidoController@paid');



Route::get('/politicas', function () {
    return view('politicas');
});

Route::get('/contactanos', function () {
    return view('contactanos');
});
