@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card card-inverse" style="background-color: #333; border-color:#333;">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <h2 class="card-title" style="color:white;">Bienvenido {{ucfirst(Auth()->user()->nombre)}}</h2>
                        <p class="card-text">
                           @if (session('status'))
                               <div class="alert alert-success"role="alert">
                                  {{ session('status') }}
                               </div>
                           @endif

                          {{ __('Estas logueado!') }}
                        </p>

                    </div>

                    <div class="col-md-4 col-sm-4 text-center">
                        <img class="btn-md" src="../imagenes/perfil/avatar.png" style="border-radius: 50%; width:40%; margin-top: 5%;">
                    </div>

                    <div class="col-md-4 col-sm-4 text-left">
                        <a href="/logout" class="btn btn-success">Cerrar sesión</a>
                        <a href="/inicio" class="btn btn-success">Inicio</a>
                    </div>
                </div>
            </div>

            </div>
        </div>


    </div>
</div>
@endsection
