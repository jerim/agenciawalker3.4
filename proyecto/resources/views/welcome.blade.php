<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- Fonts -->
       <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <style>
          html,
          body,
          header,
            #intro {
               height: 100%;
            }

            #intro {
              background: url("../imagenes/fondo/palacio.JPG")no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }

            #logo {
              width: 100px;
            }

            .top-nav-collapse {
               background-color:#86B22C;
            }

            @media (max-width: 768px) {
               .navbar:not(.top-nav-collapse) {
                  background-color:#86B22C;
                }
             }

            @media (min-width: 800px) and (max-width: 990px) {
              .navbar:not(.top-nav-collapse) {
                 background-color:#86B22C;
               }
             }

        </style>
    </head>
<body>
     <!--Main Navigation-->
    <header>

        <!--Navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">

            <div class="container">

                <!-- Navbar brand -->
                <a class="navbar-brand" href="/inicio"><img id="logo" src="../imagenes/logo/logo2.png"/>Agencia de viajes Walker</a>

                <!-- Collapse button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
                    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Collapsible content -->
                <div class="collapse navbar-collapse" id="basicExampleNav">

                    <!-- Links -->
                    <ul class="navbar-nav mr-auto smooth-scroll">
                         <li class="nav-item">
                            <a class="nav-link" href="/inicio">Inicio
                              <svg id="icono" class="bi bi-house" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                              <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 00.5.5h9a.5.5 0 00.5-.5V7h1v6.5a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 012 13.5zm11-11V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd"/>
                              <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 011.414 0l6.647 6.646a.5.5 0 01-.708.708L8 2.207 1.354 8.854a.5.5 0 11-.708-.708L7.293 1.5z" clip-rule="evenodd"/></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/agencia">Productos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/tienda">Descubre guardados
                              <svg id="icono" class="bi bi-bag" width="1em" height="1em" viewBox="  0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                              <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 001 1h10a1 1 0 001-1V5zM1 4v10a2 2 0 002 2h10a2 2 0 002-2V4H1z" clip-rule="evenodd"/>
                              <path d="M8 1.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
                               </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/politicas">Politicas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/contactanos">Contactanos</a>
                        </li>

                         <li class="nav-item">
                              @if(Route::has('login'))
                                  @auth
                                       <a class="nav-link" id="navbarDropdownMenuLink" href="{{ url('/home') }}">Tu cuenta</a>
                                  @else
                                       <a class="nav-link" id="navbarDropdownMenuLink" href="{{ route('login') }}">Inicia sesión</a>
                                  @endauth
                              @endif
                              </li>

                              <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" id="navbarDropdownMenuLink" href="{{ route('register') }}">Registrate</a>
                                @endif
                              </li>


                    </ul>
                </div>


            </div>

        </nav>

        <div id="intro" class="view">

            <div class="mask rgba-black-strong">

                <div class="container-fluid d-flex align-items-center justify-content-center h-100">

            </div>

        </div>
        <!--/.Mask-->

    </header>

   <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
   <script type="text/javascript" src='{{'js/main.js'}}'></script>
</body>
</html>
