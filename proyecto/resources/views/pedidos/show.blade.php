@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-sm-6">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Id de comprador</th>
                    <th>Usuario</th>
                    <th>Total a pagar</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$pedido->id}}</td>
                    <td>{{$pedido->user->nombre}}</td>
                    <td>{{$pedido->total()}}€</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
 <div class="col-sm-6" style="margin-top: 55px;">
      <div class="card bg-light mb-3" style="max-width: 40rem;">
        <div class="card-header">Pagar</div>
      </div>
      <div class="card-body">
        <a href="#" class="btn btn-success">Comprar</a>
      </div>
  </div>


@endsection
