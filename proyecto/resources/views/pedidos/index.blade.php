@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-sm-6">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Id de comprador</th>
                    <th>Usuario</th>
                    <th>Total a pagar</th>
                </tr>
            </thead>
            <tbody>
                @forelse($pedidos as $pedido)
                <tr>
                    <td>{{$pedido->id}}</td>
                    <td>{{$pedido->user->nombre}}</td>
                    <td> <a href="/pedidos/{{$pedido->id}}" class="btn btn-success">Detalles de compra</a></td>
                </tr>
                @empty
                   <tr>
                     <td colspan="5">¡No hay pedidos!</td>
                   </tr>
                @endforelse
            </tbody>
        </table>

    </div>
</div>



@endsection
