@extends('layouts.app')

@section('content')
@foreach($productos as $producto)
@can('create',$producto)
<a href="/agencia/create" class="btn btn-success" style="margin-left: 15px">Crear producto</a>
@endcan
<div class="card-deck" style="margin-top:5px; margin-right: 1px">
    <div class="col mb-2">
        <h4 class="card-title">{{$categorias[$producto->categoria_id-1]->nombre}}</h4>
        <div class="card h-100" style="width:270px">
            <img src="imagenes/productos/{{$producto->rutaImg}}" class="img-thumbnail" height="auto" />
                <div class="card-body">
                    <h5 class="card-title">{{$producto->nombre}} desde {{$producto->precio}} € </h5>
                    <a href="/agencia/{{$producto->id}}" class="btn btn-success">Información Viaje</a>
                    @can('update',$producto)
                    <a href="/agencia/{{$producto->id}}/edit" class="btn btn-primary">Modificar Viaje</a>
                    @endcan
                    @can('delete',$producto)
                    <form method="post" action="/agencia/{{$producto->id}}">
                      {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                    </form>
                    @endcan
                </div>
        </div>
    </div>
</div>
@endforeach
@endsection
